<?php
/**
 * Additional features to allow styling of the templates
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function pd_body_classes( $classes ) {
	// Add class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Add class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Add class if we're viewing the Customizer for easier styling of theme options.
	if ( is_customize_preview() ) {
		$classes[] = 'pd-customizer';
	}

	// Add class on front page.
	if ( is_front_page() && 'posts' !== get_option( 'show_on_front' ) ) {
		$classes[] = 'pd-front-page';
	}

	// Add a class if there is a custom header.
	if ( has_header_image() ) {
		$classes[] = 'has-header-image';
	}

	// Add class if sidebar is used.
	if ( is_active_sidebar( 'sidebar-1' ) && ! is_page() ) {
		$classes[] = 'has-sidebar';
	}

	// Add class if the site title and tagline is hidden.
	if ( 'blank' === get_header_textcolor() ) {
		$classes[] = 'title-tagline-hidden';
	}

	if ( pd_option( 'vertical_mega_menu', null, false ) && is_active_sidebar( 'vertical-mega-menu' ) ) {
		$classes[] = 'has-vertical-mega-menu';
	}

	return $classes;
}
add_filter( 'body_class', 'pd_body_classes' );

/**
 * Adds custom classes to the array of layout classes.
 *
 * @param array $classes Classes for the layout element.
 */
function pd_layout_class( $classes = array() ) {
	$classes = (array) $classes;

	if ( PD_Sidebar::has_sidebar() ) {
		$classes[] = sprintf( 'sidebar-%s', PD_Sidebar::get_sidebar_area() );
	} else {
		$classes[] = 'no-sidebar';
	}

	$classes = apply_filters( 'pd_layout_class', $classes );

	echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
}

/**
 * Primary menu fallback function.
 */
function primary_menu_fallback() {
	$classes = pd_option( 'enable_header_search', null, false ) ? 'primary-menu-container visible-lg col-md-9' : 'primary-menu-container visible-lg col-md-12';

	$fallback_menu = '<div class="' . $classes . '"><ul id="primary-menu" class="menu clearfix"><li><a href="%1$s" rel="home">%2$s</a></li></ul></div>';
	printf( $fallback_menu, esc_url( home_url( '/' ) ), esc_html__( 'Trang chủ', 'pd-theme' ) ); // WPCS: XSS OK.
}

/**
 * Mobile menu fallback function.
 */
function mobile_menu_fallback() {
	$fallback_menu = '<ul id="mobile-menu" class="mobile-menu"><li><a href="%1$s" rel="home">%2$s</a></li></ul>';
	printf( $fallback_menu, esc_url( home_url( '/' ) ), esc_html__( 'Trang chủ', 'pd-theme' ) ); // WPCS: XSS OK.
}

// Fix Seo by yoast
add_filter( 'wpseo_canonical', '__return_false' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since PD Theme 1.0
 *
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function pd_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<a class="link-more" href="%1$s" class="more-link">%2$s</a>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Đọc thêm &raquo;<span class="screen-reader-text"> "%s"</span>', 'pd-theme' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'pd_excerpt_more' );

/**
 * Disable WP emojicons.
 */
function disable_wp_emojicons() {

	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	add_filter( 'emoji_svg_url', '__return_false' );
}
add_action( 'init', 'disable_wp_emojicons' );

/**
 * Disable WP emojicons.
 *
 * @param  array $plugins //
 * @return array          //
 */
function disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove script version.
 *
 * @param  string $src //
 * @return string      //
 */
function pd_remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}
add_filter( 'script_loader_src', 'pd_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'pd_remove_script_version', 15, 1 );

if ( pd_option( 'include_fb_sdk_js', null, false ) ) {
	/**
	 * Include Facebook App ID on footer.
	 */
	function pd_include_facebook_app_id() {
		?>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/<?php pd_option( 'fb_language' ); ?>/sdk.js#xfbml=1&version=v2.7&appId=<?php pd_option( 'facebook_app_id' ); ?>";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	<?php
	}
	add_action( 'wp_footer', 'pd_include_facebook_app_id' );
}

/**
 * PD layout classes.
 *
 * @param array $classes CSS selector for own site.
 */
function pd_layout_classes( $classes = array() ) {
	$classes = (array) $classes;

	if ( 'boxed' == pd_option( 'site_layout', 'full', false ) ) {
		$classes[] = 'boxed';
	} else {
		$classes[] = 'full';
	}

	if ( '1000' == $site_width = pd_option( 'site_width', null, false ) ) {
		$classes[] = 'w1000';
	} elseif ( '1170' == $site_width ) {
		$classes[] = 'w1170';
	} elseif ( '1200' == $site_width ) {
		$classes[] = 'w1200';
	} else {
		$classes[] = 'w1000';
	}

	$classes = apply_filters( 'pd_layout_classes', $classes );

	echo 'class="' . esc_attr( join( ' ', $classes ) ) . '"';
}

/**
 * Excerpt lenght.
 *
 * @param  int $length Number to change excerpt length.
 * @return int         //
 */
function pd_excerpt_length( $length ) {
	return 60;
}
add_filter( 'excerpt_length', 'pd_excerpt_length', 999 );

/**
 * Css for our theme.
 *
 * @return string Css
 */
function pd_customizer_css_option() {
	$css= '';

	if ( '#ea2b33' != pd_option( 'main_bg_color', null, false ) ) {
		$css = '.main-navigation, .widget-title, .site-footer, .top-footer, .copyright, .close-menu {background:' . pd_option( 'main_bg_color', null, false ) . ';}';
		$css .= '.header-search .search-form .search-submit {color:' . pd_option( 'main_bg_color', null, false ) . ';}';
	}

	if ( $submenu_bg_color = pd_option( 'submenu_bg_color', null, false ) ) {
		$css .= "#primary-menu li ul.sub-menu{background:{$submenu_bg_color}}";
	}

	wp_add_inline_style( 'pd-main', $css );
}
add_action( 'wp_enqueue_scripts', 'pd_customizer_css_option', 11 );

// Support shortcodes in pd textarea widgets
add_filter('pd_textarea_widget', 'do_shortcode');

/**
 * Display back to top text
 *
 * @param  boolean $echo Echo or return value of back to top button.
 * @return string        Back to top html.
 */
function pd_back_to_top( $echo = true ) {
	if ( ! $backtotop = pd_option( 'totop', true, false ) ) {
		return;
	}

	$btt_html = '<div id="backtotop" title="' . esc_html__( 'Lên đầu trang', 'pd-theme' ) . '"><i class="fa fa-chevron-up" aria-hidden="true"></i></div>';

	$btt_html = apply_filters( 'pd_back_to_top_html', $btt_html );

	if ( ! $echo ) {
		return $btt_html;
	}

	print $btt_html; // WPCS: XSS OK.
}

if ( pd_option( 'header_script', null, false ) && pd_option( 'header_script_on_off', null, false ) ) {
	/**
	 * Adds script on wp_head.
	 *
	 * @return string //
	 */
	function pd_header_script() {
		pd_option( 'header_script' );
	}
	add_action( 'wp_head', 'pd_header_script' );
}

if ( pd_option( 'footer_script', null, false ) && pd_option( 'footer_script_on_off', null, false ) ) {
	/**
	 * Adds script on wp_footer.
	 *
	 * @return string //
	 */
	function pd_footer_script() {
		pd_option( 'footer_script' );
	}
	add_action( 'wp_footer', 'pd_footer_script' );
}

/**
 * Adds customer users.
 */
add_role( 'quantrivien', __( 'Quản trị viên', 'pd-theme' ), array(
	'switch_themes'               => 0,
	'edit_themes'                 => 0,
	'activate_plugins'            => 1,
	'edit_plugins'                => 0,
	'edit_users'                  => 1,
	'edit_files'                  => 1,
	'manage_options'              => 1,
	'moderate_comments'           => 1,
	'manage_categories'           => 1,
	'manage_links'                => 1,
	'upload_files'                => 1,
	'import'                      => 1,
	'unfiltered_html'             => 1,
	'edit_posts'                  => 1,
	'edit_others_posts'           => 1,
	'edit_published_posts'        => 1,
	'publish_posts'               => 1,
	'edit_pages'                  => 1,
	'read'                        => 1,
	'level_10'                    => 1,
	'level_9'                     => 1,
	'level_8'                     => 1,
	'level_7'                     => 1,
	'level_6'                     => 1,
	'level_5'                     => 1,
	'level_4'                     => 1,
	'level_3'                     => 1,
	'level_2'                     => 1,
	'level_1'                     => 1,
	'level_0'                     => 1,
	'edit_others_pages'           => 1,
	'edit_published_pages'        => 1,
	'publish_pages'               => 1,
	'delete_pages'                => 1,
	'delete_others_pages'         => 1,
	'delete_published_pages'      => 1,
	'delete_posts'                => 1,
	'delete_others_posts'         => 1,
	'delete_published_posts'      => 1,
	'delete_private_posts'        => 1,
	'edit_private_posts'          => 1,
	'read_private_posts'          => 1,
	'delete_private_pages'        => 1,
	'edit_private_pages'          => 1,
	'read_private_pages'          => 1,
	'delete_users'                => 0,
	'create_users'                => 1,
	'unfiltered_upload'           => 1,
	'edit_dashboard'              => 1,
	'update_plugins'              => 1,
	'delete_plugins'              => 0,
	'install_plugins'             => 0,
	'update_themes'               => 1,
	'install_themes'              => 0,
	'update_core'                 => 1,
	'list_users'                  => 1,
	'remove_users'                => 1,
	'add_users'                   => 1,
	'promote_users'               => 1,
	'edit_theme_options'          => 1,
	'delete_themes'               => 0,
	'export'                      => 1,
	'edit_comment'                => 1,
	'approve_comment'             => 1,
	'unapprove_comment'           => 1,
	'reply_comment'               => 1,
	'quick_edit_comment'          => 1,
	'spam_comment'                => 1,
	'unspam_comment'              => 1,
	'trash_comment'               => 1,
	'untrash_comment'             => 1,
	'delete_comment'              => 1,
	'edit_permalink'              => 1,
) );

/**
 * Get core capabilities
 * @return array core capabilities
 */
function pd_get_core_capabilities() {
	$capabilities = array();

	$capabilities['core'] = array(
		'manage_woocommerce',
		'manage_woocommerce_orders',
		'manage_woocommerce_coupons',
		'manage_woocommerce_products',
		'view_woocommerce_reports',
	);

	$capability_types = array( 'product', 'shop_order', 'shop_coupon', 'shop_webhook' );

	foreach ( $capability_types as $capability_type ) {

		$capabilities[ $capability_type ] = array(
			// Post type
			"edit_{$capability_type}",
			"read_{$capability_type}",
			"delete_{$capability_type}",
			"edit_{$capability_type}s",
			"edit_others_{$capability_type}s",
			"publish_{$capability_type}s",
			"read_private_{$capability_type}s",
			"delete_{$capability_type}s",
			"delete_private_{$capability_type}s",
			"delete_published_{$capability_type}s",
			"delete_others_{$capability_type}s",
			"edit_private_{$capability_type}s",
			"edit_published_{$capability_type}s",

			// Terms
			"manage_{$capability_type}_terms",
			"edit_{$capability_type}_terms",
			"delete_{$capability_type}_terms",
			"assign_{$capability_type}_terms",
		);
	}

	return $capabilities;
}

/**
 * Add role capabilities
 */
function pd_add_role_caps() {

	$capabilities = pd_get_core_capabilities();

    $role = get_role( 'quantrivien' );

	foreach ( $capabilities as $cap_group ) {
		foreach ( $cap_group as $cap ) {
		    $role->add_cap( $cap );
		}
	}

}
add_action( 'admin_init', 'pd_add_role_caps');

/**
 * Set login logo url
 * @return string http://phoenixdigi.vn.
 */
function pd_login_logo_url() {
	return 'http://phoenixdigi.vn';
}
add_filter( 'login_headerurl', 'pd_login_logo_url' );

/**
 * Set login logo url title
 * @return [type] [description]
 */
function pd_login_logo_url_title() {
	return 'CÔNG TY TNHH CÔNG NGHỆ VÀ TRUYỀN THÔNG PHOENIXDIGI VIỆT NAM - PHOENIXDIGI.VN';
}
add_filter( 'login_headertitle', 'pd_login_logo_url_title' );

/**
 * Set login logo
 */
function pd_login_logo() { ?>
	<style type="text/css">
		body {
		    color: #eb4a3a !important;
		    background: url(<?php echo get_theme_file_uri( 'assets/images/login-bg.jpg' ); ?>) center center !important;
		    background-color: rgba(0,0,0,.9) !important;
		    background-blend-mode: overlay;
		}
		a {
		    color: #f9a931 !important;
		}
		a:focus {
		    color: #f9a931;
		    -webkit-box-shadow: 0 0 0 1px #f9a931, 0 0 2px 1px #f9a931 !important;
		    box-shadow: 0 0 0 1px #f9a931, 0 0 2px 1px #f9a931 !important;
		}
		#login h1 a, .login h1 a {
			background-image: url(<?php echo get_theme_file_uri( 'assets/images/login-logo.png' ); ?>);
			height: 200px;
			width: 200px;
			background-size: 200px 200px;
		}
		.login #backtoblog a, .login #nav a {
			color: #fdc70c !important;
		}
		.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
		    color: #fed013 !important;
		}
		.wp-core-ui .button-primary {
		    background: #fdc70c !important;
		    border-color: #fdc70c !important;
		    -webkit-box-shadow: 0 1px 0 #fdc70c !important;
		    box-shadow: 0 1px 0 #fdc70c !important;
		    text-shadow: none !important;
		    border-radius: 0 !important;
		}
		input[type=text]:focus, input[type=search]:focus, input[type=radio]:focus, input[type=tel]:focus, input[type=time]:focus, input[type=url]:focus, input[type=week]:focus, input[type=password]:focus, input[type=checkbox]:focus, input[type=color]:focus, input[type=date]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=email]:focus, input[type=month]:focus, input[type=number]:focus, select:focus, textarea:focus {
		    border-color: #fdc70c !important;
		    -webkit-box-shadow: 0 0 2px rgb(253, 199, 12) !important;
		    box-shadow: 0 0 2px rgb(253, 199, 12) !important;
		}
		input[type=text]:focus, input[type=search]:focus, input[type=radio]:focus, input[type=tel]:focus, input[type=time]:focus, input[type=url]:focus, input[type=week]:focus, input[type=password]:focus, input[type=checkbox]:focus, input[type=color]:focus, input[type=date]:focus, input[type=datetime]:focus, input[type=datetime-local]:focus, input[type=email]:focus, input[type=month]:focus, input[type=number]:focus, select:focus, textarea:focus {
		    border-color: #fdc70c !important;
		    -webkit-box-shadow: 0 0 2px rgb(253, 199, 12) !important;
		    box-shadow: 0 0 2px rgb(253, 199, 12) !important;
		}
		input[type=checkbox]:checked:before {
		    content: "\f147" !important;
		    margin: -3px 0 0 -4px !important;
		    color: #fdc70c !important;
		}
		#login {
		    padding: 6% 0 0 !important;
		}Pgi
		@media screen and (max-height: 550px) {
			#login {
			    padding: 20px 0 !important;
			}
		}
		@media (max-width: 413px) {
			#login h1 a, .login h1 a {
				height: 100px;
				width: 100px;
				background-size: 100px 100px;
			}
		}
	</style>
<?php }
add_action( 'login_enqueue_scripts', 'pd_login_logo' );

/**
* Disable Default Dashboard Widgets
* @link https://digwp.com/2014/02/disable-default-dashboard-widgets/
*/
function pd_disable_default_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
}
add_action('wp_dashboard_setup', 'pd_disable_default_dashboard_widgets', 999);

// Remove Wordpress Welcome Panel
remove_action('welcome_panel', 'wp_welcome_panel');

/**
 * Remove Wordpress Version
 * @return null
 */
function pd_remove_version() {
	return;
}
add_filter('the_generator', 'pd_remove_version');

/**
 * Remove Filter Wordpress Version
 * @return [type] [description]
 */
function pd_footer_wp_version() {
    remove_filter( 'update_footer', 'core_update_footer' );
}
add_action( 'admin_menu', 'pd_footer_wp_version' );

/**
 * Filter Admin Wordpress Footer
 * @return string Description Admin Wordpress Footer
 */
function pd_remove_footer_admin () {
	_e( 'Cảm ơn bạn đã sử dụng dịch vụ của <a href="http://phoenixdigi.vn">Phoenix Digi Việt Nam</a>.', 'pd-theme' );
}
add_filter('admin_footer_text', 'pd_remove_footer_admin');
