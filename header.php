<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<?php if ( pd_option( 'responsive', true, false ) ) : ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php endif; ?>
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" <?php pd_layout_classes( 'site' ); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html__( 'Skip to content', 'pd-theme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<?php get_template_part( 'template-parts/topbar' ); ?>

		<?php get_template_part( 'template-parts/navigation', 'inline' ); ?>

		<?php do_action( 'above_content_before' ); ?>

		<?php if ( is_active_sidebar( 'above-content' ) && is_front_page() ) : ?>
		<div class="above-content-section">
			<?php if ( ! pd_option( 'above_content_full_width', null, false ) ) : ?>
			<div class="container">
				<div class="row">
			<?php endif; ?>

					<?php dynamic_sidebar( 'above-content' ); ?>

			<?php if ( ! pd_option( 'above_content_full_width', null, false ) ) : ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<?php endif; ?>

		<?php do_action( 'above_content_after' ); ?>

		<?php
		if ( is_single() || is_page() && ! is_front_page() ) {
			the_title( '<h1 class="entry-title"><div class="container">', '</div></h1>' );
		} elseif ( is_archive() ) {
			the_archive_title( '<h1 class="entry-title"><div class="container">', '</h1></div>' );
		} elseif ( is_home() ) {
			echo '<h1 class="entry-title"><div class="container">' . esc_html__( 'Tin tức', 'phoenixdigi' ) . '</div></h1>';
		}

		if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
			yoast_breadcrumb('<div id="breadcrumbs"><div class="container">','</div></div>');
		}
		?>

	</header><!-- #masthead -->

	<div id="content" class="site-content">

		<div class="container">
			<div class="row">
				<div id="layout" <?php pd_layout_class( 'clearfix' ); ?>>

					<?php do_action( 'before_layout' );
