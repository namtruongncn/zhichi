<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

?>

<aside id="secondary-2" class="sidebar widget-area">
	<figure class="instructor-avatar">
		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'instructor-avatar' ); ?></a>
	</figure>
</aside><!-- #secondary -->
