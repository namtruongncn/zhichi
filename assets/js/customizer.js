/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {
	var style = $( '#rt-color-scheme-css' ),
		api = wp.customize;

	if ( ! style.length ) {
		style = $( 'head' ).append( '<style type="text/css" id="rt-color-scheme-css" />' )
		                    .find( '#rt-color-scheme-css' );
	}
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );

	// Header text color.
    wp.customize( 'header_textcolor', function( value ) {
        value.bind( function( to ) {
            if ( 'blank' === to ) {
                $( '.site-title a, .site-description' ).css( {
                    'clip': 'rect(1px, 1px, 1px, 1px)',
                    'position': 'absolute'
                } );
            } else {
                $( '.site-title a, .site-description' ).css( {
                    'clip': 'auto',
                    'position': 'relative'
                } );
                $( '.site-title a, .site-description' ).css( {
                    'color': to
                } );
            }
        } );
    } );

	// Body Background color.
	wp.customize( 'background_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( 'body' ).css( {
					'background': 'transparent',
				} );
			} else {
				$( 'body' ).css( {
					'background': to,
				} );
			}
		} );
	} );

	// Main Background color.
	wp.customize( 'main_bg_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.main-navigation, .widget-title, .site-footer, .top-footer, .copyright, .close-menu' ).css( {
					'background': 'transparent',
				} );
			} else {
				$( '.main-navigation, .widget-title, .site-footer, .top-footer, .copyright, .close-menu' ).css( {
					'background': to,
				} );
			}
		} );
	} );

	// Text color.
	wp.customize( 'text_color', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( 'body' ).css( {
					'color': 'transparent',
				} );
			} else {
				$( 'body' ).css( {
					'color': to,
				} );
			}
		} );
	} );

    // vertical_mega_menu_title.
	wp.customize( 'vertical_mega_menu_title', function( value ) {
		value.bind( function( to ) {
		  $( '.vertical-mega-menu-title' ).text( to );
		} );
	} );

    // Copyright background color.
    wp.customize( 'copyright', function( value ) {
        value.bind( function( to ) {
            if ( '' == to ) {
                $( '.copyright' ).css({
                    display: 'none',
                });
            } else {
                $( '.copyright' ).css({
                    display: 'block',
                });
            }
        } );
    } );

	// Color Scheme CSS.
	api.bind( 'preview-ready', function() {
		api.preview.bind( 'update-color-scheme-css', function( css ) {
			style.html( css );
		} );
	} );
} )( jQuery );
