(function($) {
    "use strict";

    // backtop functions
    $( '#backtotop' ).on( 'click', function () {
      $( 'html, body' ).animate( { scrollTop: 0 }, 800 );
      return false;
    });

    $( window ).on( 'scroll', function () {
      if ( $( this ).scrollTop() > 100 ) {
        $( '#backtotop' ).fadeIn( 1000, function() {
          $( 'span' , this ).fadeIn( 100 );
        });
      } else {
        $( '#backtotop' ).fadeOut( 1000, function() {
          $( 'span' , this ).fadeOut( 100 );
        });
      }
    });
    // end function backtop

    $('.mobile-menu .menu-item-has-children').prepend('<i class="fa fa-angle-down"></i>');

    $('.mobile-menu .menu-item-has-children > i').click(function(event) {
      $(this).parent().toggleClass('active');
    });

    $('#menu-toggle, .close-menu').click(function(event) {
       $('.site').toggleClass('mobile-menu-active');
    });

    $('.pd-overlay').click(function(event) {
        $('.site').removeClass('mobile-menu-active');
    });

    $('.fixed-area .pd_fbpage_widget .widget-title').click(function(event) {
        $('.fixed-area .pd_fbpage_widget .fb-page').toggleClass('show');
    });

})(jQuery);
