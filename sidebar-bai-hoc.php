<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

$video = get_post_meta( get_the_ID(), 'video', true );
$type = get_post_meta( get_the_ID(), 'type', true );
$instructor = get_post_meta( get_the_ID(), 'instructor', true );

if ( ! empty( $type ) ) {
	if ( 'basic' === $type['type'] ) {
		$type = esc_html__( 'Miễn phí', 'phoenixdigi' );
	} elseif ( 'advanced' === $type['type'] ) {
		$type = esc_html__( 'Nâng cao', 'phoenixdigi' );
	}
}

if ( ! empty( $instructor ) ) {
	$instructor = array(
		'name'   => get_the_title( $instructor['instructor'] ),
		'link'   => get_the_permalink( $instructor['instructor'] ),
		'avatar' => get_the_post_thumbnail( $instructor['instructor'], 'instructor-avatar' ),
	);
}

?>

<aside id="secondary-2" class="sidebar widget-area">
	<div class="lesson-info">
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5819c2e7c69162fc"></script>
		<div class="addthis_inline_share_toolbox_um2f"></div>
		<h3 class="lesson-widget-title"><?php esc_html_e( 'THÔNG TIN BÀI HỌC', 'phoenixdigi' ); ?></h3>
		<div class="lesson-view-count"><?php printf( esc_html__( 'Lượt xem: %s', 'phoenixdigi' ), esc_html( pd_postview_get( get_the_ID() ) ) ); ?></div>
		<div class="lesson-category"><?php printf( esc_html__( 'Thuộc khoá học: %s', 'phoenixdigi' ), esc_html( $type ) ); ?></div>
		<div class="lesson-instructor">
			<div class="instructor-name">
				<?php printf( esc_html__( 'Giảng viên: %s', 'phoenixdigi' ), '<a href="' . esc_url( $instructor['link'] ) . '"><strong><em>' . esc_html( $instructor['name'] ) . '</em></strong></a>' ); ?>
			</div>

			<figure class="instructor-avatar">
				<a href="<?php echo esc_url( $instructor['link'] ); ?>">
					<?php print $instructor['avatar']; // WPCS: XSS OK. ?>
				</a>
			</figure>
		</div>
	</div>
</aside><!-- #secondary -->
