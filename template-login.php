<?php
/**
 * Template Name: Đăng nhập
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( ! is_user_logged_in() ) :
				wp_login_form();
			else :
				echo '<div class="user-logged-in alert alert-success">Bạn đã đang nhập!</div>';
			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
