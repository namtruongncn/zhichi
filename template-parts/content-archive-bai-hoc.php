<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'list_item col-lg-4 col-md-4 col-sm-6 col-xs-12' ); ?>>
	<div class="list_item-thumbnail">

		<?php if ( has_post_thumbnail() ) : ?>
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'medium' ); ?>
		</a>
		<div class="read-more-lessons">
			<a href="<?php the_permalink(); ?>"><?php esc_html_e( 'Chi tiết ...', 'phoenixdigi' ); ?></a>
		</div>
		<?php endif; ?>

	</div><!-- .list_item-thumbnail -->

	<div class="list_item-details">
		<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
	</div>
</article><!-- #post-## -->
