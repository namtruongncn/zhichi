<?php
/**
 * Navigation Teamplate
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 */

?>
<nav id="site-navigation" class="main-navigation">
	<div class="container">
		<div class="row">

			<div class="site-branding col-md-3 col-sm-4 col-xs-8">
				<?php if ( has_custom_logo() ) : ?>
					<?php the_custom_logo(); ?>
				<?php else : ?>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="custom-logo-link" rel="home" itemprop="url">
						<img width="205" height="83" src="<?php echo esc_url( get_theme_file_url( 'assets/images/logo.jpg' ) ); ?>" class="custom-logo" alt="Zhichi" itemprop="logo" sizes="100vw">
					</a>
				<?php endif; ?>
			</div><!-- .col-xs-4 -->

			<?php
				wp_nav_menu( array(
					'theme_location'  => 'primary',
					'menu_id'         => 'primary-menu',
					'menu_class'      => 'menu clearfix',
					'container_class' => 'primary-menu-container visible-lg col-lg-6',
					'fallback_cb'     => 'primary_menu_fallback',
				));
			?>

			<?php if ( is_active_sidebar( 'header-right' ) ) : ?>
			<div class="header-socials visible-lg col-lg-3">
				<?php dynamic_sidebar( 'header-right' ); ?>
			</div>
			<?php endif; ?>

			<div class="hidden-lg col-md-9 col-sm-8 col-xs-4">
				<button id="menu-toggle" type="button" class="pd-navbar-toggle hidden-lg">
					<span class="screen-reader-text sr-only"><?php esc_html_e( 'Toggle navigation', 'pd-theme' ); ?></span>
					<span class="icon-bar bar1"></span>
					<span class="icon-bar bar2"></span>
					<span class="icon-bar bar3"></span>
				</button>
			</div>

		</div><!-- .row -->
	</div><!-- .container -->
</nav><!-- #site-navigation -->
