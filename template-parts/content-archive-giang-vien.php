<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'list_item col-lg-3 col-md-3 col-sm-4 col-xs-12' ); ?>>

	<?php the_title( '<h4 class="entry-title text-center"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>

	<?php if ( has_post_thumbnail() ) : ?>
	<div class="instructor-avatar">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( 'full' ); ?>
		</a>
	</div>
	<?php endif; ?>

</article><!-- #post-## -->
