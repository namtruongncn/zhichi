<?php
/**
 * Footer template part.
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 */

if ( ! is_active_sidebar( 'bottom-footer' ) ) {
	return;
}

?>

<div class="bottom-footer">
	<div class="container">
		<div class="row">
			<?php if ( is_active_sidebar( 'bottom-footer' ) ) : ?>
				<div class="col-xs-12">
					<?php dynamic_sidebar( 'bottom-footer' ); ?>
				</div>
			<?php endif; ?>
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .bottom-footer -->
