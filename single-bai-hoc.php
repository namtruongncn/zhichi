<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Phoenix_Digi
 * @subpackage Phoenix_Digi
 * @since 1.0
 * @version 1.0
 */

get_header();

$video = get_post_meta( get_the_ID(), 'video', true );

if ( ! empty( $video ) ) {
	$link = substr( $video['video'], -11 );
} else {
	$link = '';
}

?>

	<?php if ( ! empty( $video ) ) : ?>
	<div class="lesson-video">
		<div class="container">
			<iframe width="100%" height="600" src="https://www.youtube.com/embed/<?php echo esc_attr( $link ); ?>" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				pd_postview_set( get_the_ID() );

				get_template_part( 'template-parts/content', 'single' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar( 'bai-hoc' );
get_footer();
